class Persoa {
    //Atributos
        nome;
        apelidos;
        #ingresos;
        #estadoCivil;
    //Construtor
        constructor (nome,apelidos, ingresos, estadoCivil){
            this.nome = nome;
            this.apelidos = apelidos;
            this.#ingresos = ingresos;
            this.#estadoCivil = estadoCivil;
        }
    //Setters (Establecemento e modificación de valores)
        //Establecemento-modificación de Ingresos
        set setIngresos (ingresosNovos){
            this.#ingresos = ingresosNovos;
        }
        //Estabelecemento-modificación de Estado Civil (EC=EsatadoCivil)
        set setEstadoCivil (novoEC){
            this.#estadoCivil = novoEC;
        }

    //Getters (Obtención ou Visualización de valores)
        //Obtención dos ingresos -devolve os ingresos, non o texto descritivo-
        get getIngresos (){
            return this.#ingresos;
        }
        //Obtención do Estado Civil -devolve o Estado Civil, non texto descritivo-
        get getEstadoCivil (){
            return this.#estadoCivil;
        }
}

class Programador extends Persoa {
    //Atributos
        #linguaxePrincipal;
        #dataAntiguidade;
    //Construtor
        constructor(nome, apelidos, ingresos,estadoCivil, linguaxePrincipal, dataAntiguidade){
            super(nome, apelidos, ingresos, estadoCivil);
            this.#linguaxePrincipal = linguaxePrincipal;
            this.#dataAntiguidade = dataAntiguidade;
        }
    //Setters (Establecemento e modificación de valores)
        //Establecer-modificar a linguaxe principal do programador
        set setLinguaxePrincipal (lP){
            this.#linguaxePrincipal = lP;
        }
        /*Establecer-modificar a data de antigüidade como programador 
           Formato:AAAAMMDD 
           AAAA=Ano (4 díxitos), 
           MM=Mes (2 díxitos, se é menor que 10, primeiro díxito=0)        
           DD=Día (2 díxitos, se é menor que 10, primeiro díxito=0) 
        */
        set setdataAntiguidade (dA){
            this.#dataAntiguidade = dA;
        } 
    //Getters (Obtención ou Visualización de valores)
        //Obter a linguaxe principal do programador
        get getLinguaxePrincipal (){
            return this.linguaxePrincipal;
        }
        /*Obter a data de Antiguidade como programador 
        Formato:AAAAMMDD 
           AAAA=Ano (4 díxitos), 
           MM=Mes (2 díxitos, se é menor que 10, primeiro díxito=0)        
           DD=Día (2 díxitos, se é menor que 10, primeiro díxito=0) 
        */
        get getDataAntiguidade (){
            return this.#dataAntiguidade;
        }
}